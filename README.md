# FutClub

Nosso Software é sobre Futsal, um esporte popular entre os jovens.
O sistema tem como principal função o cadastramento de partidas que auxiliarão os tecnicos a melhorarem a condição do time.
O técnico é seu auxiliar cadastram seus jogadores e a partida, e ápos a partida
cadastrar as informações da partida como gols, faltas e penalts.

# Funcionalidades

*  Sistema de Login e Cadastro
*  Registro de Jogadores
*  Registro das Partidas
*  Registro das Informações das Partidas
*  Atualizações de Partida, Informações da Partida e Jogadores
*  Tabela das Partidas
*  Tabela dos Detalhes de Jogadores
*  Tabela de Jogadores
*  nivel de acesso
*  Sessões


