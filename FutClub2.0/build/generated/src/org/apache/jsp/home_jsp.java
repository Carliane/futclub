package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_param_value_name_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_import_url;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_param_value_name_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_import_url = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_param_value_name_nobody.release();
    _jspx_tagPool_c_import_url.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<meta charset=\"utf-8\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>\r\n");
      out.write("\t<meta http-equiv='X-UA-Compatible' content='IE=edge'>\r\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        ");
      if (_jspx_meth_c_import_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t<<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">\r\n");
      out.write("        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css2?family=Playfair+Display:ital@1&display=swap\" rel=\"stylesheet\">\r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Julius+Sans+One&display=swap\" rel=\"stylesheet\">\r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Anton&family=Passion+One:wght@700&display=swap\" rel=\"stylesheet\">\r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital@1&display=swap\" rel=\"stylesheet\"> \r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Pathway+Gothic+One&family=Roboto+Condensed:ital@1&family=Squada+One&display=swap\" rel=\"stylesheet\">\r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Economica:ital,wght@1,700&family=Pathway+Gothic+One&display=swap\" rel=\"stylesheet\"> \r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Dosis:wght@500&family=Economica&family=Montserrat:ital,wght@1,500&display=swap\" rel=\"stylesheet\">     \r\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap\" rel=\"stylesheet\"> \r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/Home.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<nav class=\"navbar navbar-expand-md navbar-dark  col-md-12 col-xs-12 col-sm-12 col-lg-12 geral\" >\r\n");
      out.write("\t  <a class=\"navbar-brand logo Gothic\" href=\"#\" >\r\n");
      out.write("\t    <img src=\"img/FutClub(1).png\" width=\"45\" height=\"45\" class=\"d-inline-block align-top\" alt=\"\">\r\n");
      out.write("\t    FutClub\r\n");
      out.write("\t  </a>\r\n");
      out.write("\t  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#conteudoNavbarSuportado\" aria-controls=\"conteudoNavbarSuportado\" aria-expanded=\"false\" aria-label=\"Alterna navegação\">\r\n");
      out.write("\t    <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("\t  </button>\r\n");
      out.write("\r\n");
      out.write("\t  <div class=\"collapse navbar-collapse\" id=\"conteudoNavbarSuportado\">\r\n");
      out.write("\t    <ul class=\"navbar-nav mr-auto outros Economica\" >\r\n");
      out.write("\t      <li class=\"nav-item na\">\r\n");
      out.write("\t        <a class=\"nav-link\" href=\"index.jsp\">Home<span class=\"sr-only\">página atual</span></a>\r\n");
      out.write("\t      </li>\r\n");
      out.write("\t      <li class=\"nav-item na\">\r\n");
      out.write("\t        <a class=\"nav-link\" href=\"loginCadastro.jsp\">Entrar</a>\r\n");
      out.write("\t      </li>\r\n");
      out.write("\t      \r\n");
      out.write("              <a href=\"loginCadastro.jsp\"><button class=\"btn  btn-outline-warning my-2 my-sm-0 log dezoito\" href=\"loginCadastro.jsp\" role=\"button\">Registre-se</button></a>\r\n");
      out.write("\t \r\n");
      out.write("\t     \r\n");
      out.write("\t      \r\n");
      out.write("\t    </ul>\r\n");
      out.write("\t    \r\n");
      out.write("\t  </div>\r\n");
      out.write("\t</nav>\r\n");
      out.write("\t<div class=\"um\"></div>\r\n");
      out.write("\t<p class=\"text-left display-2 pa Anton\" id=\"Fo\">Planeje</p>\r\n");
      out.write("\t<p class=\"text-left display-2 pa Sans\" id=\"e\">&</p>\r\n");
      out.write("\t<p class=\"text-left display-2 pa Anton\" id=\"c\">Conquiste</p>\r\n");
      out.write("        <p class=\"text-left display-6 pa Gothic\" id=\"faca\">faça seu jogo se torna realidade<a href=\"loginCadastro.jsp\"><button class=\"btn  btn-outline-warning my-2 my-sm-0 log Economica\" id=\"sua\" href=\"loginCadastro.jsp\">Crie sua conta</button></a></p> \r\n");
      out.write("\t<div class=\"dois\"> \r\n");
      out.write("\t\t<div class=\"two\">\r\n");
      out.write("\t\t\t<hr>\r\n");
      out.write("\t\t<p class=\"text-center Anton\" id='tama' >Gerencie seu time</p>\r\n");
      out.write("\t\t<hr>\r\n");
      out.write("<div class=\"card-deck cart\">\r\n");
      out.write(" \r\n");
      out.write("\t\t<div class=\"card border-warning mb-3 max\" >\r\n");
      out.write("  <div class=\" text-center dentro\"><img src=\"img/jogador.png\" width=\"60\" height=\"60\" class=\"d-inline-block align-top\" alt=\"\"></div>\r\n");
      out.write("  <div class=\"card-body text-warning\">\r\n");
      out.write("    <h5 class=\"card-title Economica vinteoito\" >Jogadores</h5>\r\n");
      out.write("    <p class=\"card-text Montserrat dezoito\" >Cadastre seus jogadores é gerencie suas partidas</p>\r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card border-warning mb-3 max\" >\r\n");
      out.write("  <div class=\" text-center dentro\"><img src=\"img/brasão.png\" width=\"60\" height=\"60\" class=\"d-inline-block align-top\" alt=\"\"></div>\r\n");
      out.write("  <div class=\"card-body text-warning\">\r\n");
      out.write("    <h5 class=\"card-title Economica vinteoito\" >Equipes</h5>\r\n");
      out.write("    <p class=\"card-text Montserrat dezoito\" >Permita o acesso privado a equipes, com senhas geradas pelo seu tecnico</p>\r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card border-warning mb-3 max\" >\r\n");
      out.write("  <div class=\" text-center dentro\"><img src=\"img/apito.png\" width=\"60\" height=\"60\" class=\"d-inline-block align-top\" alt=\"\"></div>\r\n");
      out.write("  <div class=\"card-body text-warning\">\r\n");
      out.write("    <h5 class=\"card-title Economica vinteoito\" >Administradores</h5>\r\n");
      out.write("    <p class=\"card-text Montserrat dezoito\" >O tecnico e o Auxiliar tecnico poderão administrar as informações das partidas</p>\r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write(" \r\n");
      out.write(" \t<div class=\"two\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write(" <div class=\"tres\">\r\n");
      out.write("<div class=\"junto\">\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t<p class=\"text-center Anton black\" id=\"Aprenda\"  >Aprenda a Usar</p>\t\r\n");
      out.write("\t\t<p class=\"text-center Bebas quarenta black\" id=\"passopasso\"  >Passo a Passo</p>\r\n");
      out.write("\t\t<p class=\"text-center Economica quarenta black\" id=\"intrucao\"  >As intruções estão logo abaixo, siga e tire suas dúvidas!</p>\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t\t<div class=\"card-deck hema\" id=\"bonitao\">\r\n");
      out.write("\t\t<div class=\"card  text-center cab\" >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >1 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Faça seu Cadastro</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >Coloque nos seus devidos campos as informações necessárias.</p>\r\n");
      out.write("    <a href=\"loginCadastro.jsp\"><button type=\"button\" class=\"btn btn-outline-dark my-2 my-sm-0x\tdezoito\" href=\"loginCadastro.jsp\">Registre-se</button></a>\r\n");
      out.write("    \r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center  cab\" >\r\n");
      out.write("  <div class=\"card-body\" >\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >2 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Cadastre os jogadores do seu time</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\">utilizando o nome é a camisa de cada um, preencha os campos!</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center  cab\" >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\">3 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Cadastre sua primeira partida</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\">Para cadastrar a partida coloque os dados principais, como: o lugar, a data é o time adversario!</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"card-deck hema\"  id=\"ole\">\r\n");
      out.write("\t\t<div class=\"card text-center cab\">\r\n");
      out.write("  <div class=\"card-body \" >\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >4 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Painel de Partidas</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >Neste painel se encotra as partidas cadastradas e suas devidas informações</p>\r\n");
      out.write("   \r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center cab\" >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >5 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\">Criar Partidas</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\">No Painel de informações da Partida tem um Botão criar novas partidas é necessário apenas clica-lo para que isso aconteça</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center cab\"  >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\">6 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Painel de mais informações</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >o Painel de mais informações e uma parte em que terá os penalts, gols e etc da partida em especifico</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("<div class=\"card-deck hema \"  id=\"ole\">\r\n");
      out.write("\t\t<div class=\"card text-center cab\">\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\">7 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Modificar informações da Partida</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >Estando no Painel da Partida Especifica, click no Botão modificar informações e você será levado para um formulario e lá você pode alterar as informações</p>\r\n");
      out.write("    \r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center cab\"  >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >8 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Deletar a Partida</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >No Painel de todas as Partidas há um botão deletar ao lado de cada partida e esse botão ao clica-lo irá apagar a partida</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"card text-center cab\" >\r\n");
      out.write("  <div class=\"card-body\">\r\n");
      out.write("    <h5 class=\"card-title Bebas\" >9 Passo</h5>\r\n");
      out.write("    <h6 class=\"card-subtitle mb-2  Economica\" >Sair</h6>\r\n");
      out.write("    <p class=\"card-text Montserrat\" >Para Sair basta Clicar em Sair da Aplicação</p>\r\n");
      out.write("    \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write(" \r\n");
      out.write("\t\t\r\n");
      out.write("\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write(" <div class=\"ultima\"></div>\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t<!-- Footer -->\r\n");
      out.write("<footer class=\"page-footer font-small mdb-color pt-4\">\r\n");
      out.write("\r\n");
      out.write("  <!-- Footer Links -->\r\n");
      out.write("  <div class=\"container text-center text-md-left\">\r\n");
      out.write("\r\n");
      out.write("    <!-- Footer links -->\r\n");
      out.write("    <div class=\"row text-center text-md-left mt-3 pb-3\">\r\n");
      out.write("\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("      <div class=\"col-md-3 col-lg-3 col-xl-3 mx-auto mt-3\">\r\n");
      out.write("        <h6 class=\"text-uppercase mb-4 font-weight-bold Gothic branco quarenta\" ><img src=\"img/FutClub(1).png\" width=\"45\" height=\"45\" class=\"d-inline-block align-top\" alt=\"\">FutClub</h6>\r\n");
      out.write("        <p class=\"Economica branco vinte\" >Nosso sitema abrange seu time, nós queremos auxiliar sua partida para que seu time melhore em qualidade.</p>\r\n");
      out.write("      </div>\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("\r\n");
      out.write("     \r\n");
      out.write("\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("      <div class=\"col-md-4 col-lg-3 col-xl-3 mx-auto mt-3\">\r\n");
      out.write("        <h6 class=\"text-uppercase mb-4 font-weight-bold  Economica branco vintecinco\" >Contato</h6>\r\n");
      out.write("        \r\n");
      out.write("        <p class=\"Economica branco vinte\" >\r\n");
      out.write("          <i class=\"fa fa-envelope mr-3\" ></i> cavalcantecarliane@gmail.com</p>\r\n");
      out.write("          <p class=\"Economica branco vinte\" >\r\n");
      out.write("          <i class=\"fa fa-envelope mr-3\"></i>keylianegadelha@gmail.com</p>\r\n");
      out.write("          <p  class=\"Economica branco vinte\" >\r\n");
      out.write("          <i class=\"fa fa-envelope mr-3\"></i> otakulifecrack@gmail.com</p>\r\n");
      out.write("        \r\n");
      out.write("      </div>\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("\r\n");
      out.write("    <!-- Footer links -->\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("    <!-- Call to action -->\r\n");
      out.write("    <ul class=\"list-unstyled list-inline text-center py-2\">\r\n");
      out.write("      <li class=\"list-inline-item\">\r\n");
      out.write("        <h5 class=\"mb-1 text-center Economica branco vintetres\" >Registre-se Gratuitamente</h5>\r\n");
      out.write("      </li>\r\n");
      out.write("      <li class=\"list-inline-item\">\r\n");
      out.write("        <a href=\"loginCadastro.jsp\" class=\"btn btn-outline-white btn-rounded text-center Economica vinte\"><button class=\"btn  btn-outline-warning my-2 my-sm-0 log branco\" id=\"crie\" href=\"loginCadastro.jsp\">Crie sua conta</button></a>\r\n");
      out.write("      </li>\r\n");
      out.write("    </ul>\r\n");
      out.write("    <!-- Call to action -->\r\n");
      out.write("\r\n");
      out.write("  </div>\r\n");
      out.write("    <hr>\r\n");
      out.write("\r\n");
      out.write("    <!-- Grid row -->\r\n");
      out.write("    <div class=\"row d-flex align-items-center\">\r\n");
      out.write("\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("      <div class=\"col-md-7 col-lg-8\">\r\n");
      out.write("\r\n");
      out.write("        <!--Copyright-->\r\n");
      out.write("        <p class=\"text-center text-md-left Economica branco\" >© 2020 Copyright\r\n");
      out.write("          \r\n");
      out.write("        </p>\r\n");
      out.write("\r\n");
      out.write("      </div>\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("      <div class=\"col-md-5 col-lg-4 ml-lg-0\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Social buttons -->\r\n");
      out.write("        \r\n");
      out.write("      </div>\r\n");
      out.write("      <!-- Grid column -->\r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("    <!-- Grid row -->\r\n");
      out.write("\r\n");
      out.write("  </div>\r\n");
      out.write("  <!-- Footer Links -->\r\n");
      out.write("\r\n");
      out.write("</footer>\r\n");
      out.write("<!-- Footer -->\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_import_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:import
    org.apache.taglibs.standard.tag.rt.core.ImportTag _jspx_th_c_import_0 = (org.apache.taglibs.standard.tag.rt.core.ImportTag) _jspx_tagPool_c_import_url.get(org.apache.taglibs.standard.tag.rt.core.ImportTag.class);
    _jspx_th_c_import_0.setPageContext(_jspx_page_context);
    _jspx_th_c_import_0.setParent(null);
    _jspx_th_c_import_0.setUrl("header.jsp");
    int[] _jspx_push_body_count_c_import_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_import_0 = _jspx_th_c_import_0.doStartTag();
      if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
          out = _jspx_page_context.pushBody();
          _jspx_push_body_count_c_import_0[0]++;
          _jspx_th_c_import_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
          _jspx_th_c_import_0.doInitBody();
        }
        do {
          if (_jspx_meth_c_param_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_import_0, _jspx_page_context, _jspx_push_body_count_c_import_0))
            return true;
          out.write(' ');
          int evalDoAfterBody = _jspx_th_c_import_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
        if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
          out = _jspx_page_context.popBody();
          _jspx_push_body_count_c_import_0[0]--;
      }
      if (_jspx_th_c_import_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_import_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_import_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_import_0.doFinally();
      _jspx_tagPool_c_import_url.reuse(_jspx_th_c_import_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_param_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_import_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_import_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:param
    org.apache.taglibs.standard.tag.rt.core.ParamTag _jspx_th_c_param_0 = (org.apache.taglibs.standard.tag.rt.core.ParamTag) _jspx_tagPool_c_param_value_name_nobody.get(org.apache.taglibs.standard.tag.rt.core.ParamTag.class);
    _jspx_th_c_param_0.setPageContext(_jspx_page_context);
    _jspx_th_c_param_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_import_0);
    _jspx_th_c_param_0.setName("titulo");
    _jspx_th_c_param_0.setValue("FutClub");
    int _jspx_eval_c_param_0 = _jspx_th_c_param_0.doStartTag();
    if (_jspx_th_c_param_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_param_value_name_nobody.reuse(_jspx_th_c_param_0);
      return true;
    }
    _jspx_tagPool_c_param_value_name_nobody.reuse(_jspx_th_c_param_0);
    return false;
  }
}
