package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import controle.Conexao;

public final class TabInfoPartida_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">\n");
      out.write("        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/TabInfoPartida.css\">\n");
      out.write("        <title>Informações da Partida</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"col-lg-8 m-auto\" >\n");
      out.write("        <table class=\"table text-center mt-5\">\n");
      out.write("        <thead>\n");
      out.write("          <tr>\n");
      out.write("            <th scope=\"col\">Auxiliar</th>\n");
      out.write("            <th scope=\"col\">Gols</th>\n");
      out.write("            <th scope=\"col\">Gols do adversário</th>\n");
      out.write("            <th scope=\"col\">Faltas</th>\n");
      out.write("            <th scope=\"col\">Pênalti</th>\n");
      out.write("            \n");
      out.write("          </tr>\n");
      out.write("        </thead>\n");
      out.write("        </div>\n");
      out.write("              <tbody>\n");
      out.write("              <tr>\n");
      out.write("                 ");

                  Conexao con = new Conexao();
                  Statement ps = con.getCon().createStatement();
                  ResultSet rs = ps.executeQuery("SELECT auxiliar.nome,gols,golsAdversario,faltas,penalti FROM infojogos LEFT JOIN auxiliar ON auxiliar.idAuxiliar = infojogos.idAuxiliar;");
                  while(rs.next()){
                      out.print("<tr>");
                      out.print("<td>" + rs.getString("idPartida")+"</td>" );
                      out.print("<td>" + rs.getString("time")+"</td>" );
                      out.print("<td>" + rs.getString("timeAdversatio")+"</td>" );
                      out.print("<td>" + rs.getString("quadra")+"</td>" );
                      out.print("<td>" + rs.getString("data")+"</td>" );
                      out.print("<td>" + rs.getString("hora")+"</td>" );
                 
                     out.print("</tr>");
                  }
              
      out.write("\n");
      out.write("              \n");
      out.write("              </tr>\n");
      out.write("              </tbody>\n");
      out.write("           \n");
      out.write("    </table>\n");
      out.write("            <a href=\"TabPartida.jsp\"><button type=\"button\" class=\"btn btn-warning\">Voltar</button></a>\n");
      out.write("    </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
