package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginCadastro_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_param_value_name_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_import_url;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_param_value_name_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_import_url = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_param_value_name_nobody.release();
    _jspx_tagPool_c_import_url.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"pt-br\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"UTF-8\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n");
      out.write("     ");
      if (_jspx_meth_c_import_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/loginCadastro.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\"\r\n");
      out.write("        integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <div class=\"content first-content\">\r\n");
      out.write("            <div class=\"first-column\">\r\n");
      out.write("                <h2 class=\"title title-primary\">Bem-vindo de volta!</h2>\r\n");
      out.write("                <p class=\"description description-primary\">Por favor, insira suas informações pessoais</p>\r\n");
      out.write("                <button id=\"signin\" class=\"btn btn-primary\">Entrar</button>\r\n");
      out.write("            </div>    \r\n");
      out.write("            <div class=\"second-column\">\r\n");
      out.write("                <h2 id=\"titulo\" class=\"title title-second\">criar conta</h2>\r\n");
      out.write("                <form class=\"form\" method=\"post\" action=\"servCadastrar\">\r\n");
      out.write("                    <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"far fa-user icon-modify\"></i>\r\n");
      out.write("                        <input type=\"text\" id=\"nome\" name=\"nome\" placeholder=\"Nome\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    \r\n");
      out.write("                    \r\n");
      out.write("                    <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"far fa-user icon-modify\"></i>\r\n");
      out.write("                        <select name=\"nivel\" id=\"nivel\" required>\r\n");
      out.write("                        \t<option value=\"\" disabled selected>Nível de Acesso</option>\r\n");
      out.write("                        \t<option value=\"1\">Técnico</option>\r\n");
      out.write("                        \t<option value=\"2\">Auxiliar Técnico</option>\r\n");
      out.write("                        </select>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    \r\n");
      out.write("                    <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"far fa-envelope icon-modify\"></i>\r\n");
      out.write("                        <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"Email\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t                    \r\n");
      out.write("                    <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"fas fa-lock icon-modify\"></i>\r\n");
      out.write("                        <input type=\"password\" id=\"senha\" name=\"senha\" placeholder=\"Senha\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    \r\n");
      out.write("                     <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"fas fa-lock icon-modify\"></i>\r\n");
      out.write("                        <input type=\"password\" id=\"conSenha\" name=\"conSenha\" placeholder=\" Confirmar Senha\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    \r\n");
      out.write("                    <button id=\"btnCadastrar\" class=\"btn btn-second\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/home?pagina=loginCadastro\" >cadastrar</button>        \r\n");
      out.write("                </form>\r\n");
      out.write("            </div><!-- second column -->\r\n");
      out.write("        </div><!-- first content -->\r\n");
      out.write("        <div class=\"content second-content\">\r\n");
      out.write("            <div class=\"first-column\">\r\n");
      out.write("                <h2 class=\"title title-primary\">Registre-se</h2>\r\n");
      out.write("                <p class=\"description description-primary\"> e faça seu jogo</p>\r\n");
      out.write("                <p class=\"description description-primary\">se tornar realidade!</p>\r\n");
      out.write("                <button id=\"signup\" class=\"btn btn-primary\">cadastrar</button>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"second-column\">\r\n");
      out.write("                <h2 id=\"titulo\" class=\"title title-second\">login</h2>\r\n");
      out.write("                <form class=\"form\" method=\"post\" action=\"servLogar\">\r\n");
      out.write("                \r\n");
      out.write("                    <label class=\"label-input\" for=\"\">\r\n");
      out.write("                        <i class=\"far fa-envelope icon-modify\"></i>\r\n");
      out.write("                        <input type=\"email\" name=\"Lemail\" id=\"Lemail\" placeholder=\"Email\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    \r\n");
      out.write("                    <label class=\"label-input\">\r\n");
      out.write("                        <i class=\"far fa-user icon-modify\"></i>\r\n");
      out.write("                        <select name=\"Lnivel\" id=\"Lnivel\" required>\r\n");
      out.write("                        \t<option value=\"\" disabled selected>Nível de Acesso</option>\r\n");
      out.write("                        \t<option value=\"1\">Tecnico</option>\r\n");
      out.write("                        \t<option value=\"2\">Auxiliar Técnico</option>\r\n");
      out.write("                        </select>\r\n");
      out.write("                    </label>\r\n");
      out.write("                \r\n");
      out.write("                    <label class=\"label-input\" for=\"\">\r\n");
      out.write("                        <i class=\"fas fa-lock icon-modify\"></i>\r\n");
      out.write("                        <input type=\"password\" name=\"Lsenha\" id=\"Lsenha\" placeholder=\"Senha\" required>\r\n");
      out.write("                    </label>\r\n");
      out.write("                    <button class=\"btn btn-second\" action=\"CadastroJogador\">entrar</button>\r\n");
      out.write("                </form>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("    <script>\r\n");
      out.write("      var senha = document.getElementById('senha');\r\n");
      out.write("      var conSenha = document.getElementById('conSenha');\r\n");
      out.write("\r\n");
      out.write("      function validatePassword(){\r\n");
      out.write("        if(senha.value != conSenha.value) {\r\n");
      out.write("          conSenha.setCustomValidity('As senhas são diferentes!');\r\n");
      out.write("        } else {\r\n");
      out.write("          conSenha.setCustomValidity('');\r\n");
      out.write("        }\r\n");
      out.write("      }\r\n");
      out.write("\r\n");
      out.write("      senha.onchange = validatePassword;\r\n");
      out.write("      conSenha.onkeyup = validatePassword;\r\n");
      out.write("    </script>\r\n");
      out.write("    <script src=\"loginCadastro.js\"></script>\r\n");
      out.write("    <script src=\"loginCadastro.jsp\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_import_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:import
    org.apache.taglibs.standard.tag.rt.core.ImportTag _jspx_th_c_import_0 = (org.apache.taglibs.standard.tag.rt.core.ImportTag) _jspx_tagPool_c_import_url.get(org.apache.taglibs.standard.tag.rt.core.ImportTag.class);
    _jspx_th_c_import_0.setPageContext(_jspx_page_context);
    _jspx_th_c_import_0.setParent(null);
    _jspx_th_c_import_0.setUrl("header.jsp");
    int[] _jspx_push_body_count_c_import_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_import_0 = _jspx_th_c_import_0.doStartTag();
      if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
          out = _jspx_page_context.pushBody();
          _jspx_push_body_count_c_import_0[0]++;
          _jspx_th_c_import_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
          _jspx_th_c_import_0.doInitBody();
        }
        do {
          if (_jspx_meth_c_param_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_import_0, _jspx_page_context, _jspx_push_body_count_c_import_0))
            return true;
          out.write(' ');
          int evalDoAfterBody = _jspx_th_c_import_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
        if (_jspx_eval_c_import_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
          out = _jspx_page_context.popBody();
          _jspx_push_body_count_c_import_0[0]--;
      }
      if (_jspx_th_c_import_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_import_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_import_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_import_0.doFinally();
      _jspx_tagPool_c_import_url.reuse(_jspx_th_c_import_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_param_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_import_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_import_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:param
    org.apache.taglibs.standard.tag.rt.core.ParamTag _jspx_th_c_param_0 = (org.apache.taglibs.standard.tag.rt.core.ParamTag) _jspx_tagPool_c_param_value_name_nobody.get(org.apache.taglibs.standard.tag.rt.core.ParamTag.class);
    _jspx_th_c_param_0.setPageContext(_jspx_page_context);
    _jspx_th_c_param_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_import_0);
    _jspx_th_c_param_0.setName("titulo");
    _jspx_th_c_param_0.setValue("Registre-se");
    int _jspx_eval_c_param_0 = _jspx_th_c_param_0.doStartTag();
    if (_jspx_th_c_param_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_param_value_name_nobody.reuse(_jspx_th_c_param_0);
      return true;
    }
    _jspx_tagPool_c_param_value_name_nobody.reuse(_jspx_th_c_param_0);
    return false;
  }
}
