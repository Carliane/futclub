package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class CadastroInfo_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">\n");
      out.write("        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/CadastroInfo.css\">\n");
      out.write("        <title>Cadastrar Informações</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       <div class=\"row\">\n");
      out.write("      <div class=\"col-lg-5 m-auto\">\n");
      out.write("        <div class=\"card mt-5 bg-white\">\n");
      out.write("          <div class=\"card-title text-center mt-3\">\n");
      out.write("             <h2 class=\"h2\"> Cadastrar Informações </h2>\n");
      out.write("          </div>\n");
      out.write("          <div class=\"card-body\">\n");
      out.write("            <form method=\"post\" action=\"servInfo\" >\n");
      out.write("              <div class=\"input-group mb-3\">\n");
      out.write("                <div class=\"input-group-prepend\">\n");
      out.write("                  <span class=\"input-group-text\">\n");
      out.write("                    <i class=\"fa fa-user\"></i>\n");
      out.write("                  </span>\n");
      out.write("                </div>\n");
      out.write("                <input type=\"text\" class=\"form-control py-4\" placeholder=\"Auxiliar\" name=\"auxi\" required>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"input-group mb-3\">\n");
      out.write("                <div class=\"input-group-prepend \">\n");
      out.write("                  <span class=\"input-group-text\">\n");
      out.write("                    <i class=\"fa fa-globe\"></i>\n");
      out.write("                  </span>\n");
      out.write("                </div>\n");
      out.write("                  <input type=\"text\" class=\"form-control py-4\" placeholder=\"Gols\" name=\"gols\" required>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"input-group mb-3\">\n");
      out.write("                <div class=\"input-group-prepend \">\n");
      out.write("                  <span class=\"input-group-text\">\n");
      out.write("                   <i class=\"fa fa-globe\"></i>\n");
      out.write("                  </span>\n");
      out.write("                </div>\n");
      out.write("                  <input type=\"text\" class=\"form-control py-4\" placeholder=\"Gols do Time Adversário\" name=\"golsad\" required>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"input-group mb-3\">\n");
      out.write("                <div class=\"input-group-prepend \">\n");
      out.write("                  <span class=\"input-group-text\">\n");
      out.write("                    <i class=\"fa fa-clipboard\"></i>\n");
      out.write("                  </span>\n");
      out.write("                </div>\n");
      out.write("                  <input type=\"text\" class=\"form-control py-4\" placeholder=\"Faltas\" name=\"falta\" required>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"input-group mb-3\">\n");
      out.write("                <div class=\"input-group-prepend \">\n");
      out.write("                  <span class=\"input-group-text\">\n");
      out.write("                    <i class=\"fa fa-plus\"></i>\n");
      out.write("                  </span>\n");
      out.write("                </div>\n");
      out.write("                <input type=\"text\" class=\"form-control py-4\" placeholder=\"Pênalti\" name=\"penalti\" required>\n");
      out.write("              </div>\n");
      out.write("              <button type=\"submit\" class=\"btn btn-secondary btn-block my-3\">Enviar</button>\n");
      out.write("            </form>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("          <br>\n");
      out.write("          <a href=\"TabPartida.jsp\"><button type=\"button\" class=\"btn btn-warning\">Voltar</button></a>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
