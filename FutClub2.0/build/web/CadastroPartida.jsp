<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap" rel="stylesheet"> 
        	<link href="https://fonts.googleapis.com/css2?family=Economica:ital,wght@1,700&family=Pathway+Gothic+One&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="css/CadastroPartida.css">
        <c:import url="header.jsp" ><c:param name="titulo" value="Cadastrar Partida" ></c:param> </c:import>
    </head>
    <body>
            <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		if(email2 != null){
			response.sendRedirect("TabPart.jsp");
		}else if(email == null){
                    response.sendRedirect("loginCadastro.jsp");
                    }else{
	%>     
         <div class="row ">
      <div class="col-lg-5 m-auto">
        <div class="card mt-5 bg-white  core">
          
          <div class="card-body">
              <form method="post" action="home">
                  <input type="hidden" name="form" value="CadPart"/>
                  <h2 class="h2"> Cadastrar Partida <i class="fa fa-futbol-o cor"></i></h2> 
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-users cor"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Time" name="time" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                    <i class="fa fa-users cor"></i>
                  </span>
                </div>
                  <input type="text" class="form-control my-3 py-4" name="timead" placeholder="Time adversário" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                   <i class="fa fa-home cor"></i>
                  </span>
                </div>
                  <input type="text" class="form-control py-4" placeholder="Local" name="quadra" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                    <i class="fa fa-calendar cor"></i>
                  </span>
                </div>
                <input type="date" class="form-control my-3 py-4" placeholder="Data" name="data" required>
              </div>
                <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                    <i class="fa fa-clock-o cor"></i>
                  </span>
                </div>
                    <input type="time" class="form-control py-4" placeholder="hora" name="hora" required>
              </div>
              <button type="submit" class="btn btn-secondary btn-block my-3 cores">Enviar</button>
            </form>
          </div>
        </div>
           <br>
           <br>
      </div>
    </div>
        <%
                }
                %>
        <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
    
    </body>
</html>
