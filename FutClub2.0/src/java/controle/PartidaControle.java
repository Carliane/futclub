package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.PartidaModelo;

public class PartidaControle {
	public boolean inserirPartida(PartidaModelo part){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO partidas(time,timeAdversatio,quadra,hora,data) VALUES (?,?,?,?,?);");
			ps.setString(1, part.getTime());
			ps.setString(2, part.getTimead());
                        ps.setString(3, part.getQuadra());
                        ps.setString(4, part.getHora());
                        ps.setString(5, part.getData());
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	public ArrayList<PartidaModelo> selecionaPartida(){
            
        ArrayList<PartidaModelo> Partida = null;
        try {
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM partidas;");
            ResultSet rs = ps.executeQuery();
            if(rs != null){
               Partida = new ArrayList<PartidaModelo>();
            
            while(rs.next()) {
                PartidaModelo part = new PartidaModelo();
                part.setId(rs.getInt("idPartida"));
                part.setTime(rs.getString("time"));
                part.setTimead(rs.getString("timeadversatio"));
                part.setQuadra(rs.getString("quadra"));
                part.setData(rs.getString("data"));
                part.setHora(rs.getString("hora"));
                Partida.add(part);
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Partida;
    }
        
        public PartidaModelo selecionaUmPart(int Id){
            
       PartidaModelo Partida = null;
        try {
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM partidas WHERE idPartida=? ;");
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();
            if(rs != null){
                while(rs.next()) {
                Partida = new PartidaModelo();
                Partida.setId(rs.getInt("idPartida"));
                Partida.setTime(rs.getString("time"));
                Partida.setTimead(rs.getString("timeadversatio"));
                Partida.setQuadra(rs.getString("quadra"));
                Partida.setData(rs.getString("data"));
                Partida.setHora(rs.getString("hora"));
                
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Partida;
    }
        
  public boolean update(PartidaModelo part){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("UPDATE partidas SET  time = ?,  timeAdversatio= ?, quadra = ?, data = ?, hora = ?    WHERE idPartida = ? ;");
		ps.setString(1, part.getTime());
                ps.setString(2, part.getTimead());
                ps.setString(3, part.getQuadra());
                ps.setString(4, part.getHora());
                ps.setString(5, part.getData());
                ps.setInt(6, part.getId());
                
		if(!ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
 public  boolean delete(int id){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM jogadores WHERE idPartida = ? ;");
                PreparedStatement pt = con.getCon().prepareStatement("DELETE FROM infojogos WHERE idPartida = ? ;");
                PreparedStatement pr = con.getCon().prepareStatement("DELETE FROM partidas WHERE idPartida = ? ;");
		ps.setInt(1,id);
                pt.setInt(1,id);
                pr.setInt(1,id);
		if(!ps.execute()) {
                    if(!pt.execute()) {
                        if(!pr.execute()) {
                            retorno = true;
		
			
		}
                }
                }
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}

	
}
//