package controle;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import modelo.InfoModelo;
import modelo.JogadorModelo;
import modelo.PartidaModelo;

@WebServlet("/home")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String acao = request.getParameter("form").toString();
            switch(acao){
                case "CadPart":
                        
			PartidaModelo part = new PartidaModelo();
			
                        part.setTime(request.getParameter("time"));
                        part.setTimead(request.getParameter("timead"));
                        part.setQuadra(request.getParameter("quadra"));
			part.setHora(request.getParameter("hora"));
                        part.setData(request.getParameter("data"));
                        if(new PartidaControle().inserirPartida(part)){
                            request.getRequestDispatcher("TabPart.jsp").forward(request, response);
                        }else{
                            request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        break;
                case "CadInfo":
                        if(request.getParameter("idInfo") != null){ 
			InfoModelo info = new InfoModelo();
			info.setId(Integer.parseInt(request.getParameter("idInfo")));
                        info.setGols(request.getParameter("gols"));
                        info.setGolsAd(request.getParameter("golsad"));
			info.setFalta(request.getParameter("falta"));
                        info.setPenalti(request.getParameter("penalti"));
                        if(new InfoControle().inserirInfo(info)){
                            request.getRequestDispatcher("TabPart.jsp").forward(request, response);
                        }else{
                            request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        }
		    	
                        break;
                case "CadJog":
                        if(request.getParameter("idJog") != null){ 
			JogadorModelo ju = new JogadorModelo();
			ju.setId(Integer.parseInt(request.getParameter("idJog")));
                        ju.setNome(request.getParameter("nome"));
			ju.setNumero(request.getParameter("numeroCamisa"));
                        ju.setPosi(request.getParameter("Lnivel"));
                        if(new JogadorControle().inserirJogador(ju)){
                            request.getRequestDispatcher("TabPart.jsp").forward(request, response);
                        }else{
                            request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        }
                        break;
                case "EdJog":
                        if(request.getParameter("idJog") != null){ 
			JogadorModelo jug = new JogadorModelo();
			jug.setJog(Integer.parseInt(request.getParameter("idJog")));
                        jug.setNome(request.getParameter("nome"));
			jug.setNumero(request.getParameter("numeroCamisa"));
                        jug.setPosi(request.getParameter("Lnivel"));
                        if(new JogadorControle().update(jug)){
                            request.getRequestDispatcher("TabJog.jsp").forward(request, response);
                        }else{
                            request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        }
                        break;
                case "EditPart":
                        if(request.getParameter("idPart") != null){ 
			PartidaModelo Epart = new PartidaModelo();
			Epart.setId(Integer.parseInt(request.getParameter("idPart")));
                        Epart.setTime(request.getParameter("time"));
                        Epart.setTimead(request.getParameter("timead"));
                        Epart.setQuadra(request.getParameter("quadra"));
			Epart.setHora(request.getParameter("hora"));
                        Epart.setData(request.getParameter("data"));
                        if(new PartidaControle().update(Epart)){
                            request.getRequestDispatcher("TabPart.jsp").forward(request, response);
                        }else{
                            request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                         }
                        break;
                
                case "Remover":
                        if(request.getParameter("remo") != null){
                        if(new PartidaControle().delete(Integer.parseInt(request.getParameter("remo")))){
                            request.getRequestDispatcher("TabPart.jsp").forward(request, response);
                        }else{
                          request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        }
                        break;
                case "RemoveJog":
                        if(request.getParameter("re") != null){
                        if(new JogadorControle().delete(Integer.parseInt(request.getParameter("re")))){
                            request.getRequestDispatcher("TabJog.jsp").forward(request, response);
                        }else{
                          request.getRequestDispatcher("404.jsp").forward(request, response);
                        }
                        }
                        break;
                default:
                      request.getRequestDispatcher("404.jsp").forward(request, response);
                      break;   
		    	
                      
            }
        }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          String pagina = request.getParameter("pagina").toString();
          switch(pagina){
                  case "home":
                  request.getRequestDispatcher("home.jsp").forward(request, response);
                  break;
                  case "CadastroInfo":
                      if(request.getParameter("Id") != null){
                        PartidaModelo Para = new PartidaControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                        InfoModelo Info = new InfoControle().selecionaUmInfo(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Info",Info );
                      }
                  request.getRequestDispatcher("CadastroInfo.jsp").forward(request, response);
                  break;
                  case "CadastroJogador":
                      if(request.getParameter("Id") != null){
                      PartidaModelo Para = new PartidaControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                      }
                  request.getRequestDispatcher("CadastroJogador.jsp").forward(request, response);
                  break;
                  case "CadastroPartida":
                  request.getRequestDispatcher("CadastroPartida.jsp").forward(request, response);
                  break;
                  case "TabInfoPartida":
                      if(request.getParameter("Id") != null){
                        PartidaModelo Para = new PartidaControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                         InfoModelo Info = new InfoControle().selecionaUmInfo(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Info",Info );
                        
                        ArrayList<JogadorModelo> Joga = new JogadorControle().selecionarUmJogador(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Jogador", Joga);
                       
                       
                      }
                      
                 
                   request.getRequestDispatcher("TabInfoPartida.jsp").forward(request, response);
                  break;
                  case "TabPartida":
                   ArrayList<PartidaModelo> Partida = new PartidaControle().selecionaPartida();
                   request.setAttribute("Partida", Partida);
                   request.getRequestDispatcher("TabPartida.jsp").forward(request, response);
                  break;
                  case "TabJogadores":
                   ArrayList<JogadorModelo> Joga = new JogadorControle().selecionaJogador();
                   request.setAttribute("Jogador", Joga);
                   request.getRequestDispatcher("TabJogadores.jsp").forward(request, response);
                  break;
                  case "loginCadastro":
                  request.getRequestDispatcher("loginCadastro.jsp").forward(request, response);
                  break;
                  case "AtualizarInfo":
                  request.getRequestDispatcher("AtualizarInfo.jsp").forward(request, response);
                  break;
                  case "AtualizarPartida":
                      if(request.getParameter("Id") != null){
                        PartidaModelo Para = new PartidaControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                      }
                  request.getRequestDispatcher("AtualizarPartida.jsp").forward(request, response);
                  break;
                  case "AtualizarJogadores":
                      if(request.getParameter("Id") != null){
                      JogadorModelo Para = new JogadorControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                      }
                  request.getRequestDispatcher("AtualizarJogadores.jsp").forward(request, response);
                  break;
                  case "Remove":
                      if(request.getParameter("Id") != null){
                        PartidaModelo Par = new PartidaControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Par );
                      }
                  request.getRequestDispatcher("Remove.jsp").forward(request, response);
                  break;
                  case "RemoveJog":
                        if(request.getParameter("Id") != null){
                      JogadorModelo Para = new JogadorControle().selecionaUmPart(Integer.parseInt(request.getParameter("Id")));
                        request.setAttribute("Id",Para );
                      }
                  request.getRequestDispatcher("RemoveJog.jsp").forward(request, response);
                  break;
                  default:
                      request.getRequestDispatcher("404.jsp").forward(request, response);
                      break;
              }
        }
	}


