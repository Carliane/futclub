package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	private Connection con;
	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
	public void fecharConexao(){
		try {
			this.getCon().close();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: " + e.getMessage());
		}
	}
	public Conexao() {
		try {
			String url = "jdbc:mysql://localhost/soft?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String pwd = "";
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.setCon(DriverManager.getConnection(url,user,pwd));
		}catch(SQLException e) {
			System.out.println("Erro no Banco: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: " + e.getMessage());
		}
	}
}