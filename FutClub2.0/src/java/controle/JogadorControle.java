package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.JogadorModelo;
import java.util.List;
import java.sql.Statement;

public class JogadorControle {
	public boolean inserirJogador(JogadorModelo joga){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO jogadores (nome,numeroCamisa,posicao,idPartida) VALUES (?,?,?,?);");
			ps.setString(1, joga.getNome());
			ps.setString(2, joga.getNumero());
                        ps.setString(3, joga.getPosi());
                        ps.setInt(4, joga.getId());
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	
	 public JogadorModelo selecionaUmPart(int Id){
            
       JogadorModelo Partida = null;
        try {
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM jogadores WHERE idJogador=? ;");
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();
            if(rs != null){
                while(rs.next()) {
                Partida = new JogadorModelo();
                Partida.setId(rs.getInt("idPartida"));
                Partida.setJog(rs.getInt("idJogador"));
                Partida.setNome(rs.getString("nome"));
                Partida.setNumero(rs.getString("numeroCamisa"));
                Partida.setPosi(rs.getString("posicao"));
                
                
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Partida;
    }
        public ArrayList<JogadorModelo> selecionarUmJogador(int Id){
		 ArrayList<JogadorModelo> Jogador = null;
        	try {
               Conexao con = new Conexao();
               PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM jogadores WHERE idPartida = ?; ");
               ps.setInt(1, Id);
               ResultSet rs = ps.executeQuery();
               if(rs != null){
                   Jogador = new ArrayList<JogadorModelo>();
                while(rs.next()) {
                 JogadorModelo Jo = new JogadorModelo();
                Jo.setNome(rs.getString("nome"));
                Jo.setNumero(rs.getString("numeroCamisa"));
                Jo.setPosi(rs.getString("posicao"));
                Jo.setId(rs.getInt("idJogador"));
                 Jogador.add(Jo);
                
                
            }
            }
               }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
           return Jogador;
	}
      public ArrayList<JogadorModelo> selecionaJogador(){
            
        ArrayList<JogadorModelo> Jogador = null;
        try {
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM jogadores;");
            ResultSet rs = ps.executeQuery();
            if(rs != null){
               Jogador = new ArrayList<JogadorModelo>();
            
            while(rs.next()) {
                JogadorModelo part = new JogadorModelo();
               part.setId(rs.getInt("idPartida"));
               part.setJog(rs.getInt("idJogador"));
                part.setNome(rs.getString("nome"));
                part.setNumero(rs.getString("numeroCamisa"));
                part.setPosi(rs.getString("posicao"));
                Jogador.add(part);
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Jogador;
    }
      public boolean update(JogadorModelo Jog){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("UPDATE jogadores SET  nome = ?,numeroCamisa = ?, posicao = ?   WHERE  idJogador = ? ;");
		ps.setString(1, Jog.getNome());
                ps.setString(2, Jog.getNumero());
                ps.setString(3, Jog.getPosi());
                ps.setInt(4, Jog.getJog());
                
		if(!ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
public JogadorModelo selecionarUmJogado(int Id){
		JogadorModelo Jog = null;
        	try {
               Conexao con = new Conexao();
               PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM jogadores WHERE idJogador = ?; ");
               ps.setInt(1, Id);
               ResultSet rs = ps.executeQuery();
               if(rs != null){
                while(rs.next()) {
                Jog = new JogadorModelo();
                Jog.setNome(rs.getString("nome"));
                Jog.setNumero(rs.getString("numeroCamisa"));
                Jog.setPosi(rs.getString("posicao"));
                Jog.setId(rs.getInt("idJogador"));
                
            }
            }
               }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
           return Jog;
	}
public  boolean delete(int id){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM jogadores WHERE idJogador = ? ;");
                
		ps.setInt(1,id);
              
		if(!ps.execute()) {
                    
                            retorno = true;
		
			
		}
               
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}

}
