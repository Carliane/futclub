package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.InfoModelo;


public class InfoControle {
	public boolean inserirInfo(InfoModelo info){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO infojogos(gols,golsAdversario,faltas,penalti,idPartida) VALUES (?,?,?,?,?);");

			ps.setString(1, info.getGols());
                        ps.setString(2, info.getGolsAd());
                        ps.setString(3, info.getFalta());
                        ps.setString(4, info.getPenalti());
                        ps.setInt(5, info.getId());
                        
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	public ArrayList<InfoModelo> selecionaInfo(){
	ArrayList<InfoModelo> Info = null;
        try{
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM infoJogos;");
            ResultSet rs = ps.executeQuery();
            if(rs != null){
                Info = new ArrayList<InfoModelo>();
            
            while(rs.next()) {
                 InfoModelo part = new  InfoModelo();
                
                part.setGols(rs.getString("gols"));
                part.setGolsAd(rs.getString("golsAdversario"));
                part.setFalta(rs.getString("faltas"));
                part.setPenalti(rs.getString("penalti"));
                
                 Info.add(part);
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Info;
    }
        public InfoModelo selecionaUmInfo(int Id){
            
       InfoModelo Info = null;
        try {
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM infoJogos WHERE idPartida=? ;");
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();
            if(rs != null){
                while(rs.next()) {
                Info = new InfoModelo();
                Info.setGols(rs.getString("gols"));
                Info.setGolsAd(rs.getString("golsAdversario"));
                Info.setFalta(rs.getString("faltas"));
                Info.setPenalti(rs.getString("penalti"));
                
                
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Info;
    }

}
//