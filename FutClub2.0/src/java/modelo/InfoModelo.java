
package modelo;

public class InfoModelo {
    private int Id;
    private String gols;
    private String golsAd;
    private String falta;
    private String penalti;
    
     public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

   

    public String getGols() {
        return gols;
    }

    public void setGols(String gols) {
        this.gols = gols;
    }

    public String getGolsAd() {
        return golsAd;
    }

    public void setGolsAd(String golsAd) {
        this.golsAd = golsAd;
    }

    public String getFalta() {
        return falta;
    }

    public void setFalta(String falta) {
        this.falta = falta;
    }

    public String getPenalti() {
        return penalti;
    }

    public void setPenalti(String penalti) {
        this.penalti = penalti;
    }
    
    
}
