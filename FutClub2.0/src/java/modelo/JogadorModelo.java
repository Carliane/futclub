package modelo;

public class JogadorModelo {
	private int id;
        private int jog; 
	private String nome;
	private String numero;
        private String posi;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
        public int getJog() {
		return jog;
	}
	public void setJog(int jog) {
		this.jog = jog;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
        public String getPosi() {
		return posi;
	}
	public void setPosi(String posi) {
		this.posi = posi;
	}
}
