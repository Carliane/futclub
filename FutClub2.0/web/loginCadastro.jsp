<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <c:import url="header.jsp" ><c:param name="titulo" value="Registre-se" ></c:param> </c:import>
    <link rel="stylesheet" href="css/loginCadastro.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="content first-content">
            <div class="first-column">
                <h2 class="title title-primary">Bem-vindo de volta!</h2>
                <p class="description description-primary">Por favor, insira suas informa��es pessoais</p>
                <button id="signin" class="btn btn-primary">Entrar</button>
            </div>    
            <div class="second-column">
                <h2 id="titulo" class="title title-second">criar conta</h2>
                <form class="form" method="post" action="servCadastrar">
                    <label class="label-input">
                        <i class="far fa-user icon-modify"></i>
                        <input type="text" id="nome" name="nome" placeholder="Nome" required>
                    </label>
                    
                    
                    <label class="label-input">
                        <i class="far fa-user icon-modify"></i>
                        <select name="nivel" id="nivel" required>
                        	<option value="" disabled selected>N�vel de Acesso</option>
                        	<option value="1">T�cnico</option>
                        	<option value="2">Auxiliar T�cnico</option>
                        </select>
                    </label>
                    
                    <label class="label-input">
                        <i class="far fa-envelope icon-modify"></i>
                        <input type="email" id="email" name="email" placeholder="Email" required>
                    </label>
					
					                    
                    <label class="label-input">
                        <i class="fas fa-lock icon-modify"></i>
                        <input type="password" id="senha" name="senha" placeholder="Senha" required>
                    </label>
                    
                     <label class="label-input">
                        <i class="fas fa-lock icon-modify"></i>
                        <input type="password" id="conSenha" name="conSenha" placeholder=" Confirmar Senha" required>
                    </label>
                    
                    <button id="btnCadastrar" class="btn btn-second" href="${pageContext.request.contextPath}/home?pagina=loginCadastro" >cadastrar</button>        
                </form>
            </div><!-- second column -->
        </div><!-- first content -->
        <div class="content second-content">
            <div class="first-column">
                <h2 class="title title-primary">Registre-se</h2>
                <p class="description description-primary"> e fa�a seu jogo</p>
                <p class="description description-primary">se tornar realidade!</p>
                <button id="signup" class="btn btn-primary">cadastrar</button>
            </div>
            <div class="second-column">
                <h2 id="titulo" class="title title-second">login</h2>
                <form class="form" method="post" action="servLogar">
                
                    <label class="label-input" for="">
                        <i class="far fa-envelope icon-modify"></i>
                        <input type="email" name="Lemail" id="Lemail" placeholder="Email" required>
                    </label>
                    
                    <label class="label-input">
                        <i class="far fa-user icon-modify"></i>
                        <select name="Lnivel" id="Lnivel" required>
                        	<option value="" disabled selected>N�vel de Acesso</option>
                        	<option value="1">Tecnico</option>
                        	<option value="2">Auxiliar T�cnico</option>
                        </select>
                    </label>
                
                    <label class="label-input" for="">
                        <i class="fas fa-lock icon-modify"></i>
                        <input type="password" name="Lsenha" id="Lsenha" placeholder="Senha" required>
                    </label>
                    <button class="btn btn-second" action="CadastroJogador">entrar</button>
                </form>
            </div>
        </div>
    </div>
    <script>
      var senha = document.getElementById('senha');
      var conSenha = document.getElementById('conSenha');

      function validatePassword(){
        if(senha.value != conSenha.value) {
          conSenha.setCustomValidity('As senhas s�o diferentes!');
        } else {
          conSenha.setCustomValidity('');
        }
      }

      senha.onchange = validatePassword;
      conSenha.onkeyup = validatePassword;
    </script>
    <script src="loginCadastro.js"></script>
    <script src="loginCadastro.jsp"></script>
</body>
</html>