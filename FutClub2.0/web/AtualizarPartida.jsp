<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap" rel="stylesheet"> 
        	<link href="https://fonts.googleapis.com/css2?family=Economica:ital,wght@1,700&family=Pathway+Gothic+One&display=swap" rel="stylesheet"> 
    
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/CadastroPartida.css">
        <c:import url="header.jsp" ><c:param name="titulo" value="Atualizar Partida" ></c:param> </c:import>
    </head>
    <body>
    <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		if(email2 != null){
			response.sendRedirect("TabPart.jsp");
		}else if(email == null){
                    response.sendRedirect("loginCadastro.jsp");
                    }else{
	%>     
         <div class="row">
      <div class="col-lg-5 m-auto">
        <div class="card mt-5 bg-white core">
          
          <div class="card-body">
           
            <c:choose>
                <c:when test="${not empty requestScope['Id']}">
                    <c:set var="Id" value="${requestScope['Id']}"></c:set>
                </c:when>
            </c:choose>
               
             
              <form method="post" action="home">
                  <h2 class="h2"> Atualizar Partida <i class="fa fa-futbol-o cor"></i></h2>
                  <input type="hidden" name="form" value="EditPart"/>
                  <input type="hidden" name="idPart" value="${Id.id}"/>
             
                  <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-users cor"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Time" value="${Id.time}" name="time" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                    <i class="fa fa-users cor"></i>
                  </span>
                </div>
                  <input type="text" class="form-control my-3 py-4" name="timead" value="${Id.timead}" placeholder="Time adversário" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                   <i class="fa fa-home cor"></i>
                  </span>
                </div>
                  <input type="text" class="form-control py-4" placeholder="Local" value="${Id.quadra}" name="quadra" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                    <i class="fa fa-calendar cor"></i>
                  </span>
                </div>
                <input type="text" class="form-control my-3 py-4" placeholder="Data" name="data" value="${Id.data}" required>
              </div>
                <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                    <i class="fa fa-clock-o cor"></i>
                  </span>
                </div>
                    <input type="text" class="form-control py-4" placeholder="hora" name="hora" value="${Id.hora}" required>
              </div>
              <button type="submit" class="btn btn-secondary btn-block my-3 cores">Enviar</button>
            </form>
          </div>
        </div>
           
            
      </div>
    </div>
             <%
                }
                %>
    </body>
</html>
