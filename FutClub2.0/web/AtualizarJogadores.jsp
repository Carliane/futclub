<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap" rel="stylesheet"> 
        	<link href="https://fonts.googleapis.com/css2?family=Economica:ital,wght@1,700&family=Pathway+Gothic+One&display=swap" rel="stylesheet"> 
    
    <link rel="stylesheet" href="css/CadastroJogador.css">
        <c:import url="header.jsp" ><c:param name="titulo" value="Atualizar Jogador" ></c:param> </c:import>
    </head>
    <body>
          <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		if(email2 != null){
			response.sendRedirect("TabPart.jsp");
		}else if(email == null){
                    response.sendRedirect("loginCadastro.jsp");
                    }else{
	%>     
               <nav class="navbar navbar-dark bg-dark bg-transparent">
      <a class="navbar-brand logo Gothic" href="#" >
	    <img src="img/FutClub(1).png" width="45" height="45" class="d-inline-block align-top" alt="">
	    FutClub
	  </a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Alternar de navegação">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse collapse" id="navbarsExample01" style="">
        <ul class="navbar-nav mr-auto">
          
          <li class="nav-item active">
            <a class="nav-link" href="${pageContext.request.contextPath}/home?pagina=CadastroPartida"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cadastrar Partida</font></font></a>
          </li>
         
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tabelas</font></font></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="${pageContext.request.contextPath}/home?pagina=TabPartida"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ver Partidas</font></font></a>
              <a class="dropdown-item" href="${pageContext.request.contextPath}/home?pagina=TabJogadores"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ver Jogadores</font></font></a>
            </div>
          </li>
           </li>
          <li class="nav-item active">
            <a class="nav-link" href="deslogar.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sair</font></font><span class="sr-only"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(atual)</font></font></span></a>
          </li>
        </ul>
      </div>
    </nav>
        <div class="row">
      <div class="col-lg-5 m-auto">
        <div class="card mt-5 bg-white core">
        
          <div class="card-body">
              <c:choose>
                <c:when test="${not empty requestScope['Id']}">
                    <c:set var="Id" value="${requestScope['Id']}"></c:set>
                                     </c:when>
            </c:choose>
            <form method="post" action="home">
                <h2 class="h2"> Atualizar Jogador <i class="fa fa-futbol-o cor"></i></h2>
                <input type="hidden" name="form" value="EdJog"/>
                <input type="hidden" name="idJog" value="${Id.jog}"/>
            <input type="hidden" name="id" value="${Id.id}"/>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                   <i class="fa fa-user cor"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" name="nome" value="${Id.nome}" placeholder="Nome do Jogador" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                     <i class="fa fa-question cor"></i>
                  </span>
                </div>
                <input type="number" class="form-control my-3 py-4" value="${Id.numero}" name="numeroCamisa" placeholder="Número da camisa" required>
              </div>
                <div class="input-group ">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                     <i class="fa fa-map-marker cor"></i>
                  </span>
                </div>
                        <select name="Lnivel" id="Lnivel"  class="form-control" required>
                        	<option value="${Id.posi}" disabled selected></option>
                        	<option value="Fixo">Fixo</option>
                        	<option value="Pivo">Pivo</option>
                                <option value="Ala Direita">Ala Direita</option>
                                <option value="Ala Esquerda">Ala Esquerda</option>
                                <option value="Goleiro">Goleiro</option>
                        </select>
               
                </div>

            <button type="submit" class="btn btn-secondary btn-block my-3 cores">Enviar</button>
            
            </form>
          </div>
        </div>
          
          
      </div>
    </div>
                                <%
                }
                %>
                                <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
    
    </body>
</html>
