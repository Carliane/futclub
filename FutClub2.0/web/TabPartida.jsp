<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="controle.Conexao"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Anton&family=Passion+One:wght@700&display=swap" rel="stylesheet">
        	<link href="https://fonts.googleapis.com/css2?family=Pathway+Gothic+One&family=Roboto+Condensed:ital@1&family=Squada+One&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="css/TabPartida.css">
        <link rel="stylesheet" type="text/css" href="css/app.css" />
        <c:import url="header.jsp" ><c:param name="titulo" value="Partidas" ></c:param> </c:import>
    </head>
    <body>
         <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		 if(email == null && email2 == null){
                    response.sendRedirect("loginCadastro.jsp");
                    }else{
	%>
         <nav class="navbar navbar-dark bg-dark bg-transparent">
      <a class="navbar-brand logo Gothic" href="#" >
	    <img src="img/FutClub(1).png" width="45" height="45" class="d-inline-block align-top" alt="">
	    FutClub
	  </a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Alternar de navegação">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse collapse" id="navbarsExample01" style="">
        <ul class="navbar-nav mr-auto">
          
          <li class="nav-item active">
            <a class="nav-link" href="${pageContext.request.contextPath}/home?pagina=CadastroPartida"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cadastrar Partida</font></font></a>
          </li>
         
         
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tabelas</font></font></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="${pageContext.request.contextPath}/home?pagina=TabPartida"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ver Partidas</font></font></a>
              <a class="dropdown-item" href="${pageContext.request.contextPath}/home?pagina=TabJogadores"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ver Jogadores</font></font></a>
            </div>
          </li>
           </li>
          <li class="nav-item active">
            <a class="nav-link" href="deslogar.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sair</font></font><span class="sr-only"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(atual)</font></font></span></a>
          </li>
        </ul>
      </div>
    </nav>
      <div class="grid-container cont">
  			<div class="grid-x grid-margin-x">
  				<div class="cell small-10 ">
  					
  						<h3>Partidas</h3>
                                                
  						<table class="hover stack table">
  							<thead>
                                                            <tr class="cima">
  									<th>Id</th>
  									<th>Time</th>
  									<th>Time Adversario</th>
  									<th>Quadra</th>
  									<th>Data</th>
                                                                        <th>Hora</th>
                                                                        <th>Editar</th>
                                                                        <th>Remover</th>
                                                                        <th>Ver+</th>
  								</tr>
  							</thead>
  							<tbody>
                                                <c:set var="Partida" value="${requestScope['Partida']}"></c:set>
            
            <c:forEach items="${Partida}" var="Part" >
  								<tr>
  									<td><c:out value="${Part.id}"></c:out></td>
  									<td><c:out value="${Part.time}"></c:out></td>
  									<td><c:out value="${Part.timead}"></c:out></td>
                                                                        <td><c:out value="${Part.quadra}"></c:out></td>
                                                                        <td><c:out value="${Part.data}"></c:out></td>
                                                                        <td><c:out value="${Part.hora}"></c:out></td>
                                                                        
  									<td><a href="${pageContext.request.contextPath}/home?pagina=AtualizarPartida&Id=${Part.id}" class="editar"><i class="fi-pencil warning"></i></a></td>
  									<td><a href="${pageContext.request.contextPath}/home?pagina=Remove&Id=${Part.id}" class="remover"><i class="fa fa-trash-o"></i></a></td>
                                                                        <td><a href="${pageContext.request.contextPath}/home?pagina=TabInfoPartida&Id=${Part.id}" class="list"><i class="fi-list"></i></a></td>
            
                                                                        </tr>
  								
  							</c:forEach>	
  							</tbody>
  							
  						</table>
  					
  				</div>
  			</div>
  		</div>
        <div  class=" botao">
    
         <a href="${pageContext.request.contextPath}/home?pagina=CadastroPartida"><button type="submit" class="btn btn-outline-warning my-3 bot" href="CadastroPartida.jsp">Cadastrar Partida </button></a>
         
        </div>
                                                <%
                }
                %>
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
    
  </body>
</html>
