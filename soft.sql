CREATE SCHEMA soft;
CREATE TABLE soft.tecnico(
	idTecnico INT AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	senha VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	PRIMARY KEY(idTecnico)
);
CREATE TABLE soft.nivel(
	idNivel INT AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	PRIMARY KEY(idNivel)
);
CREATE TABLE soft.jogadores(
	idJogador INT AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	numeroCamisa INT(2) NOT NULL,
	PRIMARY KEY(idJogador)
);
CREATE TABLE soft.partidas(
	idPartida INT AUTO_INCREMENT,
	quadra VARCHAR(255) NOT NULL,
	data VARCHAR(10) NOT NULL,
	hora VARCHAR(10) NOT NULL,
	timeAdversatio VARCHAR (255) NOT NULL,
	PRIMARY KEY(idPartida)
);
CREATE TABLE soft.agendarPartida(
	idAgenda INT AUTO_INCREMENT,
	idTecnico INT NOT NULL,
	idJogador INT NOT NULL,
	idPartida INT NOT NULL,
	PRIMARY KEY(idAgenda),
	FOREIGN KEY(idTecnico) REFERENCES tecnico(idTecnico),
	FOREIGN KEY(idJogador) REFERENCES jogadores(idJogador),
	FOREIGN KEY(idPartida) REFERENCES partidas(idPartida)
);
CREATE TABLE soft.auxiliar(
	idAuxiliar INT AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	senha VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	PRIMARY KEY(idAuxiliar)
);
CREATE TABLE soft.infoJogos(
	idInfoJogo INT AUTO_INCREMENT,
	idAuxiliar INT NOT NULL,
	idAgenda INT NOT NULL,
	gols INT(10) NOT NULL,
	golsAdversario INT(10) NOT NULL,
	PRIMARY KEY(idInfoJogo),
	FOREIGN KEY(idAuxiliar) REFERENCES auxiliar(idAuxiliar),
	FOREIGN KEY(idAgenda) REFERENCES agendarPartida(idAgenda)
);
INSERT INTO soft.nivel(nome) VALUES ("Jogador"),("Tecnico"),("Auxiliar Tecnico");
