package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.TecnicoModelo;

public class TecnicoControle {
	public boolean inserirTecnico(TecnicoModelo tec){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO tecnico (nome,email,senha) VALUES (?,?,?);");
			ps.setString(1, tec.getNome());
			ps.setString(2, tec.getEmail());
			ps.setString(3, tec.getSenha());
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	public boolean login(TecnicoModelo tec) {
		boolean resultado = false;
        	try {
               Conexao con = new Conexao();
               PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM tecnico WHERE email = ? AND senha = ?; ");
               ps.setString(1, tec.getEmail());
               ps.setString(2, tec.getSenha());
               if(ps.execute()){
	               ResultSet rs = ps.executeQuery();
	               if(rs != null) {
	            	   while(rs.next()){
	            		   resultado = true;
	                   }
	               }
               }
               }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
           return resultado;
	}
	public boolean selecionarUm(TecnicoModelo tec){
		boolean resultado = false;
        	try {
               Conexao con = new Conexao();
               PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM tecnico WHERE email = ?; ");
               ps.setString(1, tec.getEmail());
               if(ps.execute()){
	               ResultSet rs = ps.executeQuery();
	               if(rs != null) {
	            	   while(rs.next()){
	            		   resultado = true;
	                   }
	               }
               }
               }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
           return resultado;
	}
}
