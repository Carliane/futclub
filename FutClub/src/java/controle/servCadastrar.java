package controle;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.TecnicoModelo;
import modelo.AuxiliarModelo;

@WebServlet("/servCadastrar")
public class servCadastrar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public servCadastrar() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		if(request.getParameter("nivel").equals("1")) {
			String email = request.getParameter("email");
			TecnicoModelo user = new TecnicoModelo();
			TecnicoControle use = new TecnicoControle();
		    user.setEmail(email);
		    if(use.selecionarUm(user)){
		    	out.println("<script>alert('Este email j� est� cadastrado! Fa�a login!')</script>");
		    }else{
		    	user.setNome(request.getParameter("nome"));
				user.setEmail(request.getParameter("email"));
				user.setSenha(request.getParameter("senha"));
		    	use.inserirTecnico(user);
		    	out.println("<script>alert('Cadastro realizado com sucesso!!!')</script>");
			}
		}else{
			String email = request.getParameter("email");
			AuxiliarModelo user = new AuxiliarModelo();
			AuxiliarControle use = new AuxiliarControle();
		    user.setEmail(email);
		    if(use.selecionarUm(user)){
		    	out.println("<script>alert('Este email j� est� cadastrado! Fa�a login!')</script>");
		    }else{
		    	user.setNome(request.getParameter("nome"));
				user.setEmail(request.getParameter("email"));
				user.setSenha(request.getParameter("senha"));
		    	use.inserirAuxiliar(user);
		    	out.println("<script>alert('Cadastro realizado com sucesso!!!')</script>");
			}
		}
	}
	
}
