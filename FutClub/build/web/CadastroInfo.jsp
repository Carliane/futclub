
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/CadastroInfo.css">
        <title>Cadastrar Informações</title>
    </head>
    <body>
       <div class="row">
      <div class="col-lg-5 m-auto">
        <div class="card mt-5 bg-white">
          <div class="card-title text-center mt-3">
             <h2 class="h2"> Cadastrar Informações </h2>
          </div>
          <div class="card-body">
            <form >
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-user"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Auxiliar" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                    <i class="fa fa-globe"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Gols" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                   <i class="fa fa-globe"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Gols do Time Adversário" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                    <i class="fa fa-clipboard"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Faltas" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend ">
                  <span class="input-group-text">
                    <i class="fa fa-plus"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Pênalti" required>
              </div>
              <button type="submit" class="btn btn-secondary btn-block my-3">Enviar</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    </body>
</html>
