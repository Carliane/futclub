
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/CadastroJogador.css">
        <title>Cadastro Jogador</title>
    </head>
    <body>
        <div class="row">
      <div class="col-lg-5 m-auto">
        <div class="card mt-5 bg-white">
          <div class="card-title text-center mt-3">
            <img src="img/2.png" width="120px" height="120px">
             <h2 class="h2"> Cadastrar Jogadores </h2>
          </div>
          <div class="card-body">
            <form >
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                   <i class="fa fa-user"></i>
                  </span>
                </div>
                <input type="text" class="form-control py-4" placeholder="Nome do Jogador" required>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend my-3">
                  <span class="input-group-text">
                     <i class="fa fa-question"></i>
                  </span>
                </div>
                <input type="text" class="form-control my-3 py-4" placeholder="Número da camisa" required>
              </div>

            <button type="submit" class="btn btn-secondary btn-block my-3">Enviar</button>
            
            </form>
          </div>
        </div>
          <a href="CadastroPartida.jsp"><button type="submit" class="btn btn-outline-warning my-3" href="CadastroPartida.jsp">Cadastrar Partida </button></a>
          
      </div>
    </div>

    </body>
</html>
