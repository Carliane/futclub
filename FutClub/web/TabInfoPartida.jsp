
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/TabInfoPartida.css">
        <title>Informações da Partida</title>
    </head>
    <body>
        <div class="col-lg-8 m-auto" >
        <table class="table text-center mt-5">
        <thead>
          <tr>
            <th scope="col">Auxiliar</th>
            <th scope="col">Gols</th>
            <th scope="col">Gols do adversário</th>
            <th scope="col">Faltas</th>
            <th scope="col">Pênalti</th>
            <th scope="col" colspan="2">Extra</th>
          </tr>
        </thead>
        </div>
              <tbody>
              <tr>
                <td>Jenifer</td>
                <td>3 </td>
                <td>0</td>
                <td>2</td>
                <td>0</td>
                <td><button type="button" class="btn btn-outline-warning">Atualizar</button></td>
                <td><button type="button" class="btn btn-outline-warning">Deletar</button></td>
              </tr>
              </tbody>
           
    </table>
            <a href="TabPartida.jsp"><button type="button" class="btn btn-warning">Voltar</button></a>
    </div>
    </body>
</html>
