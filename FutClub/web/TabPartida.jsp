
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/TabPartida.css">
        <title>Tabela Partidas</title>
    </head>
    <body>
      <div class="col-lg-8 m-auto" >
        <table class="table text-center mt-5">
        <thead>
          <tr>
            <th scope="col">ID Partida</th>
            <th scope="col">Time</th>
            <th scope="col">Time adversário</th>
            <th scope="col">Quadra</th>
            <th scope="col">Data</th>
            <th scope="col" colspan="2">Extra</th>
          </tr>
        </thead>
     <div class="col-sm-8">
              <tbody>
              <tr>
                <td>1</td>
                <td>Brasil </td>
                <td>Alemanha</td>
                <td>Quadra da escola</td>
                <td>11/05/2020</td>
                <td><a href="TabInfoPartida.jsp"><button type="button" class="btn btn-outline-warning">Informações</button></a></td>
                <td><a href="TabJogadores.jsp"><button type="button" class="btn btn-outline-warning">Jogadores</button></a></td>
              </tr>
              </tbody>
              </div>
    </table>
    
         <a href="CadastroPartida.jsp"><button type="submit" class="btn btn-outline-warning my-3" href="CadastroPartida.jsp">Cadastrar Partida </button></a>
          <a href="index.jsp"><button type="submit" class="btn btn-outline-warning my-3" href="index.jsp">Sair </button></a>
  </div>
  </body>
</html>
