<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="sortcut icon" href="img/FutClub(1).png" type="image/png" />
	<title>FutClub</title>
	<<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital@1&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Julius+Sans+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Anton&family=Passion+One:wght@700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital@1&display=swap" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css2?family=Pathway+Gothic+One&family=Roboto+Condensed:ital@1&family=Squada+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Economica:ital,wght@1,700&family=Pathway+Gothic+One&display=swap" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css2?family=Dosis:wght@500&family=Economica&family=Montserrat:ital,wght@1,500&display=swap" rel="stylesheet">     
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:ital@1&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="css/Home.css">
</head>
<body>
	<nav class="navbar navbar-expand-md navbar-dark  col-md-12 col-xs-12 col-sm-12 col-lg-12 geral" >
	  <a class="navbar-brand logo Gothic" href="#" >
	    <img src="img/FutClub(1).png" width="45" height="45" class="d-inline-block align-top" alt="">
	    FutClub
	  </a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navega��o">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
	    <ul class="navbar-nav mr-auto outros Economica" >
	      <li class="nav-item na">
	        <a class="nav-link" href="index.jsp">Home<span class="sr-only">p�gina atual</span></a>
	      </li>
	      <li class="nav-item na">
	        <a class="nav-link" href="loginCadastro.jsp">Entrar</a>
	      </li>
	      
              <a href="loginCadastro.jsp"><button class="btn  btn-outline-warning my-2 my-sm-0 log dezoito" href="loginCadastro.jsp" role="button">Registre-se</button></a>
	 
	     
	      
	    </ul>
	    
	  </div>
	</nav>
	<div class="um"></div>
	<p class="text-left display-2 pa Anton" id="Fo">Planeje</p>
	<p class="text-left display-2 pa Sans" id="e">&</p>
	<p class="text-left display-2 pa Anton" id="c">Conquiste</p>
        <p class="text-left display-6 pa Gothic" id="faca">fa�a seu jogo se torna realidade<a href="loginCadastro.jsp"><button class="btn  btn-outline-warning my-2 my-sm-0 log Economica" id="sua" href="loginCadastro.jsp">Crie sua conta</button></a></p> 
	<div class="dois"> 
		<div class="two">
			<hr>
		<p class="text-center Anton" id='tama' >Gerencie seu time</p>
		<hr>
<div class="card-deck cart">
 
		<div class="card border-warning mb-3 max" >
  <div class=" text-center dentro"><img src="img/jogador.png" width="60" height="60" class="d-inline-block align-top" alt=""></div>
  <div class="card-body text-warning">
    <h5 class="card-title Economica vinteoito" >Jogadores</h5>
    <p class="card-text Montserrat dezoito" >Cadastre seus jogadores � gerencie suas partidas</p>
  </div>
</div>
<div class="card border-warning mb-3 max" >
  <div class=" text-center dentro"><img src="img/bras�o.png" width="60" height="60" class="d-inline-block align-top" alt=""></div>
  <div class="card-body text-warning">
    <h5 class="card-title Economica vinteoito" >Equipes</h5>
    <p class="card-text Montserrat dezoito" >Permita o acesso privado a equipes, com senhas geradas pelo seu tecnico</p>
  </div>
</div>
<div class="card border-warning mb-3 max" >
  <div class=" text-center dentro"><img src="img/apito.png" width="60" height="60" class="d-inline-block align-top" alt=""></div>
  <div class="card-body text-warning">
    <h5 class="card-title Economica vinteoito" >Administradores</h5>
    <p class="card-text Montserrat dezoito" >O tecnico e o Auxiliar tecnico poder�o administrar as informa��es das partidas</p>
  </div>
</div>
	</div>

	</div>
</div>
 
 	<div class="two">
			</div>
 <div class="tres">
<div class="junto">
			
		<p class="text-center Anton black" id="Aprenda"  >Aprenda a Usar</p>	
		<p class="text-center Bebas quarenta black" id="passopasso"  >Passo a Passo</p>
		<p class="text-center Economica quarenta black" id="intrucao"  >As intru��es est�o logo abaixo, siga e tire suas d�vidas!</p>
	
	
		<div class="card-deck hema" id="bonitao">
		<div class="card  text-center cab" >
  <div class="card-body">
    <h5 class="card-title Bebas" >1 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Fa�a seu Cadastro</h6>
    <p class="card-text Montserrat" >Coloque nos seus devidos campos as informa��es necess�rias.</p>
    <a href="loginCadastro.jsp"><button type="button" class="btn btn-outline-dark my-2 my-sm-0x	dezoito" href="loginCadastro.jsp">Registre-se</button></a>
    
    
  </div>
</div>
<div class="card text-center  cab" >
  <div class="card-body" >
    <h5 class="card-title Bebas" >2 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Cadastre os jogadores do seu time</h6>
    <p class="card-text Montserrat">utilizando o nome � a camisa de cada um, preencha os campos!</p>
    
  </div>
</div>
<div class="card text-center  cab" >
  <div class="card-body">
    <h5 class="card-title Bebas">3 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Cadastre sua primeira partida</h6>
    <p class="card-text Montserrat">Para cadastrar a partida coloque os dados principais, como: o lugar, a data � o time adversario!</p>
    
  </div>
</div>

		</div>
	


<div class="card-deck hema"  id="ole">
		<div class="card text-center cab">
  <div class="card-body " >
    <h5 class="card-title Bebas" >4 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Painel de Partidas</h6>
    <p class="card-text Montserrat" >Neste painel se encotra as partidas cadastradas e suas devidas informa��es</p>
   
    
  </div>
</div>
<div class="card text-center cab" >
  <div class="card-body">
    <h5 class="card-title Bebas" >5 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica">Criar Partidas</h6>
    <p class="card-text Montserrat">No Painel de informa��es da Partida tem um Bot�o criar novas partidas � necess�rio apenas clica-lo para que isso aconte�a</p>
    
  </div>
</div>
<div class="card text-center cab"  >
  <div class="card-body">
    <h5 class="card-title Bebas">6 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Painel de mais informa��es</h6>
    <p class="card-text Montserrat" >o Painel de mais informa��es e uma parte em que ter� os penalts, gols e etc da partida em especifico</p>
    
  </div>
</div>

		</div>

<div class="card-deck hema "  id="ole">
		<div class="card text-center cab">
  <div class="card-body">
    <h5 class="card-title Bebas">7 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Modificar informa��es da Partida</h6>
    <p class="card-text Montserrat" >Estando no Painel da Partida Especifica, click no Bot�o modificar informa��es e voc� ser� levado para um formulario e l� voc� pode alterar as informa��es</p>
    
    
  </div>
</div>
<div class="card text-center cab"  >
  <div class="card-body">
    <h5 class="card-title Bebas" >8 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Deletar a Partida</h6>
    <p class="card-text Montserrat" >No Painel de todas as Partidas h� um bot�o deletar ao lado de cada partida e esse bot�o ao clica-lo ir� apagar a partida</p>
    
  </div>
</div>
<div class="card text-center cab" >
  <div class="card-body">
    <h5 class="card-title Bebas" >9 Passo</h5>
    <h6 class="card-subtitle mb-2  Economica" >Sair</h6>
    <p class="card-text Montserrat" >Para Sair basta Clicar em Sair da Aplica��o</p>
    
  </div>
</div>
 
		
	</div>
	</div>

</div>
<div class="quarta">
	<div class="four">

	<p class="text-center Economica black quarenta" id="Desenvolvedoras" >Desenvolvedoras</p>
	<hr class="hr">
	<div class="card-deck dev">
		<div class=" text-center" >
  <div class="card-body">
    <div class="bombom" id='carli'></div>
    <h6 class="card-subtitle mb-2 Economica nome" >Carliane Cavalcante Barros</h6>
     <h6 class="card-subtitle mb-2 text-muted Economica funcao"  >Desenvolvedora & Desing</h6>

     
    
   
    
  </div>
</div>
<div class="text-center" >
    <div class="card-body">
   <div class="bombom" id='jeni'> </div>
    <h6 class="card-subtitle mb-2 Economica nome"  >Jenifer Bezzera dos Santos</h6>
     <h6 class="card-subtitle mb-2 text-muted Economica funcao" >Desenvovedora</h6>
    
    
  </div>
</div>
<div class=" text-center" >
    <div class="card-body">
   <div class="bombom " id='key'></div>
    <h6 class="card-subtitle mb-2 Economica nome" >Keyliane Gadelha</h6>
     <h6 class="card-subtitle mb-2 text-muted Economica funcao" >Desenvolvedora</h6>
   
  </div>
</div>
    </div>

		</div>
	</div>

</div>
 <div class="ultima"></div>
 


	<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Footer links -->
    <div class="row text-center text-md-left mt-3 pb-3">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold Gothic branco quarenta" ><img src="img/FutClub(1).png" width="45" height="45" class="d-inline-block align-top" alt="">FutClub</h6>
        <p class="Economica branco vinte" >Nosso sitema abrange seu time, n�s queremos auxiliar sua partida para que seu time melhore em qualidade.</p>
      </div>
      <!-- Grid column -->

     

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold  Economica branco vintecinco" >Contato</h6>
        
        <p class="Economica branco vinte" >
          <i class="fas fa-envelope mr-3" ></i> cavalcantecarliane@gmail.com</p>
          <p class="Economica branco vinte" >
          <i class="fas fa-envelope mr-3"></i>keylianegadelha@gmail.com</p>
          <p  class="Economica branco vinte" >
          <i class="fas fa-envelope mr-3"></i> otakulifecrack@gmail.com</p>
        
      </div>
      <!-- Grid column -->

    </div>

    <!-- Footer links -->
<div class="container">

    <!-- Call to action -->
    <ul class="list-unstyled list-inline text-center py-2">
      <li class="list-inline-item">
        <h5 class="mb-1 text-center Economica branco vintetres" >Registre-se Gratuitamente</h5>
      </li>
      <li class="list-inline-item">
        <a href="loginCadastro.jsp" class="btn btn-outline-white btn-rounded text-center Economica vinte"><button class="btn  btn-outline-warning my-2 my-sm-0 log branco" id="crie" href="loginCadastro.jsp">Crie sua conta</button></a>
      </li>
    </ul>
    <!-- Call to action -->

  </div>
    <hr>

    <!-- Grid row -->
    <div class="row d-flex align-items-center">

      <!-- Grid column -->
      <div class="col-md-7 col-lg-8">

        <!--Copyright-->
        <p class="text-center text-md-left Economica branco" >� 2020 Copyright
          
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-5 col-lg-4 ml-lg-0">

        <!-- Social buttons -->
        
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

</footer>
<!-- Footer -->
</body>
</html>